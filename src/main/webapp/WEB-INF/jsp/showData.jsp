<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<body>
<div>
    <div>
        <h1>WordPress SSO OAuth Integration Example - Data Received</h1>
        <h2>
            Received data: <br>
            ID: ${userData.ID} <br>
            userLogin: ${userData.userLogin} <br>
            userNiceName: ${userData.userNiceName} <br>
            userEmail: ${userData.userEmail} <br>
            userRegistered: ${userData.userRegistered} <br>
            userStatus: ${userData.userStatus} <br>
            displayName: ${userData.displayName} <br>
            email: ${userData.email} <br>

        </h2>

    </div>
</div>
</body>
</html>