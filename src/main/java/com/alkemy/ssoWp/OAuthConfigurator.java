package com.alkemy.ssoWp;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class OAuthConfigurator {

    @Value("${wordpress.sso.url}")
    private String wordpressUrl;

    @Value("${wordpress.sso.clientid}")
    private String clientId;

    @Value("${wordpress.sso.clientsecret}")
    private String clientSecret;

    @Value("${wordpress.sso.callbackurl}")
    private String callbackUrl;


    public String getWordpressUrl() {
        return wordpressUrl;
    }

    public String getClientId() {
        return clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }
}
