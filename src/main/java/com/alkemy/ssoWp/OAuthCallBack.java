package com.alkemy.ssoWp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Random;
import java.util.Scanner;

@Controller
public class OAuthCallBack {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private OAuthConfigurator oAuthConfigurator;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping(path = "/OAuthCallBack", method = RequestMethod.GET)
    public String createIndex(@RequestParam(required = true) String code, @RequestParam(required = true) String state, Map<String, Object> model) {
        WordpressUser returner = null;
        try {

            WpOAuthApi.instance().setUrl(oAuthConfigurator.getWordpressUrl());

            final OAuth20Service service = new ServiceBuilder(oAuthConfigurator.getClientId())
                    .apiSecret(oAuthConfigurator.getClientSecret())
                    .state(state)
                    .callback(oAuthConfigurator.getCallbackUrl())
                    .build(WpOAuthApi.instance());

            final OAuth2AccessToken accessToken = service.getAccessToken(code);
            final OAuthRequest request = new OAuthRequest(Verb.GET, oAuthConfigurator.getWordpressUrl() + "/oauth/me/");
            service.signRequest(accessToken, request);
            final Response response = service.execute(request);
            if (response.getCode() == 200) {
                returner = objectMapper.readValue(response.getBody(), WordpressUser.class);
            }


        } catch (Exception ex) {
            logger.error(ex.toString());
        }

        if (returner != null) {
            logger.info("Username:" + returner.getUserLogin());
            model.put("userData",returner);
        }


        return "showData";

    }

}

