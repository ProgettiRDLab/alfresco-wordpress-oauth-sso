package com.alkemy.ssoWp;

import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.oauth.OAuth20Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;
import java.util.Random;

@Controller
public class IndexController {

    @Autowired
    private OAuthConfigurator oAuthConfigurator;

    @GetMapping("/")
    public String home(Map<String, Object> model) {
        final String secretState = "secret" + new Random().nextInt(999_999);

        WpOAuthApi.instance().setUrl(oAuthConfigurator.getWordpressUrl());

        final OAuth20Service service = new ServiceBuilder(oAuthConfigurator.getClientId())
                .apiSecret(oAuthConfigurator.getClientSecret())
                .state(secretState)
                .callback(oAuthConfigurator.getCallbackUrl())
                .build(WpOAuthApi.instance());

        model.put("oauthUrl", service.getAuthorizationUrl());
        return "index";
    }

}
