package com.alkemy.ssoWp;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class WordpressUser {

    //{"ID":"1",
    // "user_login":"admin",
    // "user_nicename":"admin",
    // "user_email":"rossella.carnevale@alkemy.com",
    // "user_registered":"2018-05-21 13:46:34",
    // "user_status":"0",
    // "display_name":"admin",
    // "email":"rossella.carnevale@alkemy.com"}

    @JsonProperty("ID")
    private Long ID;

    @JsonProperty("user_login")
    private String userLogin;

    @JsonProperty("user_nicename")
    private String userNiceName;

    @JsonProperty("user_email")
    private String userEmail;

    @JsonProperty("user_registered")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date userRegistered;

    @JsonProperty("user_status")
    private Integer userStatus;

    @JsonProperty("display_name")
    private String displayName;

    @JsonProperty("email")
    private String email;

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getUserNiceName() {
        return userNiceName;
    }

    public void setUserNiceName(String userNiceName) {
        this.userNiceName = userNiceName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Date getUserRegistered() {
        return userRegistered;
    }

    public void setUserRegistered(Date userRegistered) {
        this.userRegistered = userRegistered;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
