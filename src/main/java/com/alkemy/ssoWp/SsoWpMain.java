package com.alkemy.ssoWp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@SpringBootApplication
@ComponentScan(basePackages = "com.alkemy.ssoWp")
@EnableWebMvc
public class SsoWpMain extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(SsoWpMain.class, args);
    }

    @Override
    public SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SsoWpMain.class);
    }


}